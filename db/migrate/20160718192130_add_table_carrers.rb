class AddTableCarrers < ActiveRecord::Migration
  create_table :carrers do |t|
    t.string :name
    t.string :email
    t.string :phone
    t.text   :message

    t.timestamps null: false
  end
end

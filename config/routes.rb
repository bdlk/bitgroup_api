Rails.application.routes.draw do
  
  devise_for :admin_users, ActiveAdmin::Devise.config
  
  ActiveAdmin.routes(self)
  
  root 'home#index'

  namespace :api do
    namespace :v1 do
      resources :banners, only: [:index, :show]
      resources :branches, only: [:index, :show]
      resources :categories, only: [:index, :show]
      resources :firms, only: [:index, :show]
      resources :products, only: [:index, :show]
      resources :socials, only: [:index, :show]
      resources :corporations, only: [:index, :show]
      resources :contents, only: [:index, :show]
      resources :carrers, only: [:index]
      resources :contacts, only: [:index]
      resources :newsletters, only: [:index]
    end
  end

end

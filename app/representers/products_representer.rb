module ProductsRepresenter
  include Roar::JSON
  include Roar::Hypermedia
  
  collection :items, extend: ProductRepresenter, as: "products"
end

module SocialRepresenter
  include Roar::JSON
  include Roar::Hypermedia
  
  property :id
  property :icon
  property :title
  property :url

  link :self do
    
  end
end

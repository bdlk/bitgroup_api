module CorporationsRepresenter
  include Roar::JSON
  include Roar::Hypermedia
  
  collection :items, extend: CorporationRepresenter, as: "corporations"
end

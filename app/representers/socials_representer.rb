module SocialsRepresenter
  include Roar::JSON
  include Roar::Hypermedia
  
  collection :items, extend: SocialRepresenter, as: "socials"
end

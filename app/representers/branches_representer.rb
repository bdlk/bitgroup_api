module BranchesRepresenter
  include Roar::JSON
  include Roar::Hypermedia
  
  collection :items, extend: BrancheRepresenter, as: "branches"
end

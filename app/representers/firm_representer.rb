module FirmRepresenter
  include Roar::JSON
  include Roar::Hypermedia
  
  property :id
  property :title  
  property :description
  property :avatar, extend: AssetRepresenter

  link :self do
    
  end
end

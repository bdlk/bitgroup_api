module FirmsRepresenter
  include Roar::JSON
  include Roar::Hypermedia
  
  collection :items, extend: FirmRepresenter, as: "firms"
end

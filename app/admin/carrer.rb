ActiveAdmin.register Carrer do

  permit_params :name, :email, :phone, :message
  menu parent: "Site", priority: 1, label: "Carrers"

  filter :name

  index do
    selectable_column
    column :id
    column :name
    column :email
    actions
  end

  form do |f|
    f.inputs 'Carrers' do
      f.input :name
      f.input :email
      f.input :phone
      f.input :avatar, :as => :file
    end
    f.button 'Save'
  end

end

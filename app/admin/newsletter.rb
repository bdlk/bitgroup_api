ActiveAdmin.register Newsletter do

  permit_params :email, :name
  menu parent: "Site", priority: 1, label: "Newsletter"

  filter :email
  filter :name

  index do
    selectable_column
    column :id
    column :email
    column :name
    actions
  end

  form do |f|
    f.inputs 'Newsletter' do
      f.input :email
      f.input :name
    end
    f.button 'Save'
  end

end

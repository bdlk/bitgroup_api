ActiveAdmin.register Firm do

  permit_params :title, :description, :avatar
  menu parent: "Site", priority: 1, label: "About us"

  filter :title
  filter :description

  index do
    selectable_column
    column :id
    column :title
    actions
  end

  form do |f|
    f.inputs 'About us' do
      f.input :title
      f.input :description, as: :html_editor
      f.input :avatar, :as => :file
    end
    f.button 'Save'
  end


end

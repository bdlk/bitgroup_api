class Api::V1::FirmsController < Api::V1::BaseController
  
  include Roar::Rails::ControllerAdditions
  include Roar::Rails::ControllerAdditions::Render
  
  def index
    firms = Firm.all
    collection = Collection.new(firms)

    render json: collection, represent_with: FirmsRepresenter,  status: 200
  end

  def show
    firm = Firm.find(params[:id])

    render json: firm, represent_with: FirmRepresenter,  status: 200
  end
end
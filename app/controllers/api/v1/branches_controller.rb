class Api::V1::BranchesController < Api::V1::BaseController
  
  include Roar::Rails::ControllerAdditions
  include Roar::Rails::ControllerAdditions::Render

  def index
    branches = Branche.all
    collection = Collection.new(branches)

    render json: collection, represent_with: BranchesRepresenter,  status: 200
  end

  def show
    branche = Branche.find(params[:id])

    render json: branche, represent_with: BrancheRepresenter,  status: 200
  end
end